package flamo.MonkKiller.src.main;


import flamo.MonkKiller.src.util.data.Location;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class GuiFrame extends JFrame {

    private MonkKillerScript script;
    private Location monastery;
    private double lowestHpPercent;
    private double desiredHpPercent;

    public GuiFrame(MonkKillerScript script) {
        initComponents();
        this.script = script;
    }

    @SuppressWarnings("unchecked")
    private void initComponents() {
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                script.stop(false);
                dispose();
            }
        });

        lowHpSpinner = new JSpinner();
        jLabel1 = new JLabel();
        jLabel2 = new JLabel();
        jLabel3 = new JLabel();
        jLabel4 = new JLabel();
        jLabel5 = new JLabel();
        desiredHpSpinner = new JSpinner();
        locationCombobox = new JComboBox<>();
        startButton = new JButton();

        setTitle("Monk Killer");
        lowHpSpinner.setAutoscrolls(true);
        jLabel1.setText("Location:");
        jLabel2.setText("Heal/Eat when character is below");
        jLabel3.setText("% hp");
        jLabel3.setToolTipText("");
        jLabel4.setText("Heal/Eat until character has");
        jLabel5.setText("% hp");
        startButton.setText("Start");
        lowHpSpinner.setModel(new SpinnerNumberModel(50, 10, 90, 5));
        desiredHpSpinner.setModel(new SpinnerNumberModel(80, 20, 100, 5));
        String[] locationNames = new String[Location.values().length];
        for (int i = 0; i < Location.values().length; i++) {
            locationNames[i] = Location.values()[i].getName();
        }
        locationCombobox.setModel(new DefaultComboBoxModel<>(locationNames));
        lowHpSpinner.addChangeListener(e -> {
            int healValue = Integer.parseInt(lowHpSpinner.getValue().toString());
            if (Integer.parseInt(desiredHpSpinner.getValue().toString()) <= healValue) {
                desiredHpSpinner.setValue(healValue + 10);
            }
        });

        desiredHpSpinner.addChangeListener(e -> {
            int healValue = Integer.parseInt(lowHpSpinner.getValue().toString());
            if (Integer.parseInt(desiredHpSpinner.getValue().toString()) <= healValue) {
                desiredHpSpinner.setValue(healValue + 10);
            }
        });

        startButton.addActionListener(e -> {
            for (Location loc : Location.values()) {
                if (loc.getName().equals(locationCombobox.getSelectedItem().toString())) {
                    monastery = loc;
                }
            }
            lowestHpPercent = (double) Integer.parseInt(lowHpSpinner.getValue().toString()) / 100;
            desiredHpPercent = (double) Integer.parseInt(desiredHpSpinner.getValue().toString()) / 100;
            if (monastery != null && lowestHpPercent != 0 && desiredHpPercent != 0) {
                setVisible(false);
            }
        });

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addComponent(jLabel1)
                                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(locationCombobox, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addComponent(jLabel2)
                                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(lowHpSpinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(jLabel3))
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addComponent(jLabel4)
                                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(desiredHpSpinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(jLabel5)))
                                                .addGap(0, 0, Short.MAX_VALUE))
                                        .addComponent(startButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel1)
                                        .addComponent(locationCombobox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel2)
                                        .addComponent(lowHpSpinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel3))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel4)
                                        .addComponent(desiredHpSpinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel5))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(startButton)
                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pack();
        for (Frame frame : Frame.getFrames()) {
            if (frame.getTitle() != null && frame.getTitle().contains("OSBot")) {
                setLocation(frame.getX() + frame.getWidth() / 2 - getWidth() / 2,  frame.getY() + frame.getHeight() / 2 - getHeight() / 2);
                break;
            }
        }
    }

    public Location getMonastery() {
        return monastery;
    }

    public double getLowestHpPercent() {
        return lowestHpPercent;
    }

    public double getDesiredHpPercent() {
        return desiredHpPercent;
    }

    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JComboBox<String> locationCombobox;
    private JSpinner desiredHpSpinner;
    private JSpinner lowHpSpinner;
    private JButton startButton;

}