package flamo.MonkKiller.src.main;

import org.osbot.rs07.api.ui.RS2Widget;
import org.osbot.rs07.api.ui.Skill;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.script.ScriptManifest;
import org.osbot.rs07.utility.ConditionalSleep;

import java.awt.*;

@ScriptManifest(
        logo = "Combat",
        name = "Easy's Monk Killer",
        info = "Kills monks.",
        author = "Easy",
        version = 0.06
)

public class MonkKillerScript extends Script {

    private final Skill[] skillsToTrack = new Skill[]{
            Skill.ATTACK,
            Skill.STRENGTH,
            Skill.DEFENCE,
            Skill.RANGED,
            Skill.MAGIC,
            Skill.HITPOINTS,
    };

    private Painter painter = new Painter(this, getName(), getVersion(), skillsToTrack);
    private GuiFrame gui = new GuiFrame(this);
    private StateHandler stateHandler;

    @Override
    public void onStart() {
        for (Skill skill : skillsToTrack) {
            getExperienceTracker().start(skill);
        }
        gui.setVisible(true);
    }

    @Override
    public int onLoop() throws InterruptedException {
        if (stateHandler == null && !gui.isVisible()) {
            stateHandler = new StateHandler(this, gui.getMonastery(), gui.getLowestHpPercent(), gui.getDesiredHpPercent());
        } else if (stateHandler != null){
            stateHandler.handleNextState();
        }
        return 100;
    }

    public void onExit() {
        if (gui != null) gui.dispose();
    }

    public void onPaint(Graphics2D g) {
        if (stateHandler == null) {
            if (gui.isVisible()) {
                painter.setStatus("Waiting for gui input");
            } else {
                painter.setStatus("Initializing");
            }
        } else {
            painter.setStatus(stateHandler.getStatus());
        }
        painter.paintTo(g);
    }

}
