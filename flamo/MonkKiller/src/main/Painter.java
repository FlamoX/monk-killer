package flamo.MonkKiller.src.main;

import flamo.MonkKiller.src.util.Format;
import flamo.MonkKiller.src.util.Timer;
import org.osbot.rs07.api.ui.Skill;
import org.osbot.rs07.script.MethodProvider;

import java.awt.*;

public class Painter {

    private final Font font = new Font("Lucida Sans Unicode", Font.PLAIN, 11);
    private final Timer timer = new Timer();
    private final int x = 4;
    private final int y = 248;
    private final int spacing = 15;

    private Skill[] skillsToTrack;
    private MethodProvider api;
    private double version;
    private String status;
    private String name;
    private int lines;

    public Painter(MethodProvider methodProvider, String name, double version, Skill[] skillsToTrack) {
        this.skillsToTrack = skillsToTrack;
        this.name = name;
        this.version = version;
        api = methodProvider;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void paintTo(Graphics2D g) {
        RenderingHints antialiasing = new RenderingHints(
                RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON
        );
        g.setRenderingHints(antialiasing);
        lines = 0;
        g.setFont(font);
        g.setColor(Color.WHITE);
        g.drawString("[" + name + " v" + version + "]", x, nextY());
        g.drawString("Runtime: " + timer.toString(), x, nextY());
        g.drawString("Status: " + status, x, nextY());
        g.drawString("--------------EXPERIENCE TRACKING--------------", x, nextY());
        for (Skill skill : skillsToTrack) {
            if (api.getExperienceTracker().getGainedXP(skill) > 0) {
                g.drawString(
                        skill.name().charAt(0) + skill.name().substring(1).toLowerCase() + ": " +
                                api.getSkills().getStatic(skill) +
                                " (" + api.getExperienceTracker().getGainedLevels(skill) + ")" +
                                " - Exp: " + api.getExperienceTracker().getGainedXP(skill) +
                                " (" + api.getExperienceTracker().getGainedXPPerHour(skill) + ")" +
                                " - TTL: " + Format.msToString(api.getExperienceTracker().getTimeToLevel(skill)),
                         x, nextY()
                );
            }
        }
    }

    private int nextY() {
        return y + spacing * (lines++ - 1);
    }

}
