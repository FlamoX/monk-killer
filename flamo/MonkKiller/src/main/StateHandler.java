package flamo.MonkKiller.src.main;

import flamo.MonkKiller.src.util.data.Location;
import org.osbot.rs07.api.filter.ActionFilter;
import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.api.ui.RS2Widget;
import org.osbot.rs07.event.WalkingEvent;
import org.osbot.rs07.event.WebWalkEvent;
import org.osbot.rs07.event.webwalk.PathPreferenceProfile;
import org.osbot.rs07.input.mouse.InventorySlotDestination;
import org.osbot.rs07.utility.Condition;
import org.osbot.rs07.utility.ConditionalSleep;
import org.osbot.rs07.script.MethodProvider;
import org.osbot.rs07.api.model.Character;
import org.osbot.rs07.api.filter.Filter;
import org.osbot.rs07.api.model.NPC;
import org.osbot.rs07.api.ui.Skill;

public class StateHandler {

    private final ActionFilter foodFilter = new ActionFilter("Eat");
    private final int maxNpcDist = 6;

    private String status = "Initializing";
    private MethodProvider api;
    private Location monkLocation;
    private double lowestHpPercent;
    private double desiredHpPercent;
    private boolean healToDesiredHp;
    private int minimumHealth;

    public StateHandler(MethodProvider methodProvider, Location monkLocation, double lowestHpPercent, double desiredHpPercent) {
        api = methodProvider;
        this.monkLocation = monkLocation;
        this.lowestHpPercent = lowestHpPercent;
        this.desiredHpPercent = desiredHpPercent;
    }

    private enum State {
        ENABLING_AUTO_RETALIATE,
        WALKING_TO_MONASTERY,
        ATTACKING_MONK,
        HEALING,
        EATING,
        IDLE
    }

    private State getState() {
        Character opponent = api.myPlayer().getInteracting();
        minimumHealth = (int) (api.getSkills().getStatic(Skill.HITPOINTS) * lowestHpPercent);
        if (monkLocation.getArea().contains(api.myPlayer())) {
            boolean needsToHeal = healToDesiredHp || api.getSkills().getDynamic(Skill.HITPOINTS) <= minimumHealth;
            if (!api.getCombat().isAutoRetaliateOn()) {
                return State.ENABLING_AUTO_RETALIATE;
            } else if (needsToHeal && api.getInventory().getAmount(foodFilter) > 0) {
                return State.EATING;
            } else if (opponent != null && (opponent.isUnderAttack() || opponent.getHealthPercent() > 0)
                    || api.myPlayer().isUnderAttack()) {
                return State.IDLE;
            } else if (needsToHeal) {
                return State.HEALING;
            } else {
                return State.ATTACKING_MONK;
            }
        }
        return State.WALKING_TO_MONASTERY;
    }

    public void handleNextState() throws InterruptedException {
        State state = getState();
        status = state.name().charAt(0) + state.name().substring(1).toLowerCase().replaceAll("_", " ");
        switch (state) {
            case ENABLING_AUTO_RETALIATE:
                api.getCombat().toggleAutoRetaliate(true);
                break;
            case WALKING_TO_MONASTERY:
                WebWalkEvent webEvent = new WebWalkEvent(monkLocation.getArea());
                PathPreferenceProfile pathProfile = new PathPreferenceProfile();
                pathProfile.checkBankForItems(true);
                pathProfile.checkEquipmentForItems(true);
                pathProfile.checkInventoryForItems(true);
                pathProfile.setAllowTeleports(true);
                webEvent.setPathPreferenceProfile(pathProfile);
                api.execute(webEvent);
                break;
            case ATTACKING_MONK:
                NPC monk = api.getNpcs().closest((Filter<NPC>) npc -> npc.getName().equals("Monk")  && npc.getInteracting() == null && npc.getHealthPercent() > 0);
                interactNPC(monk, "Attack", new ConditionalSleep(7000) {
                    @Override
                    public boolean condition() throws InterruptedException {
                        Character opponent = api.myPlayer().getInteracting();
                        Character monkInteracting = monk.getInteracting();
                        return opponent != null && (opponent.isUnderAttack() || isDeathAnimating(opponent)) || monkInteracting != null && !monkInteracting.getName().equals(api.myPlayer().getName()) && api.myPlayer().isUnderAttack();
                    }
                });
                break;
            case EATING: {
                    int playerHp = api.getSkills().getDynamic(Skill.HITPOINTS);
                    int maxPlayerHp = api.getSkills().getStatic(Skill.HITPOINTS);
                    if (playerHp <= minimumHealth) {
                        healToDesiredHp = true;
                    } else if (playerHp >= maxPlayerHp * desiredHpPercent) {
                        healToDesiredHp = false;
                        break;
                    }
                    Item food = api.getInventory().getItem(foodFilter);
                    if (food != null && food.interact("Eat")) {
                        new ConditionalSleep(6000) {
                            @Override
                            public boolean condition() throws InterruptedException {
                                return api.getSkills().getDynamic(Skill.HITPOINTS) > playerHp;
                            }
                        }.sleep();
                    }

                }
                break;
            case HEALING:
                int playerHp = api.getSkills().getDynamic(Skill.HITPOINTS);
                int maxPlayerHp = api.getSkills().getStatic(Skill.HITPOINTS);
                if (playerHp <= minimumHealth) {
                    healToDesiredHp = true;
                } else if (playerHp >= maxPlayerHp * desiredHpPercent) {
                    healToDesiredHp = false;
                    break;
                }
                if (api.getDialogues().inDialogue() ) {
                    api.getDialogues().completeDialogue("Can you heal me? I'm injured.");
                } else {
                    NPC healMonk = api.getNpcs().closest((Filter<NPC>) npc -> npc.getName().equals("Monk") && !npc.isUnderAttack() || npc.getName().equals("Abbot Langley"));
                    interactNPC(healMonk, "Talk-to", new ConditionalSleep(5000) {
                        @Override
                        public boolean condition() throws InterruptedException {
                            return api.getDialogues().inDialogue();
                        }
                    });
                }
                break;
            case IDLE:
                api.sleep(25);
                break;
        }
    }

    private void interactNPC(NPC npc, String action, ConditionalSleep conditionalSleep) {
        if (npc != null) {
            if (api.myPlayer().getPosition().distance(npc) <= maxNpcDist) {
                if (!npc.isOnScreen()) {
                    api.getCamera().toEntity(npc);
                }
                if (npc.interact(action)) {
                    conditionalSleep.sleep();
                }
            } else {
                WalkingEvent walkEvent = new WalkingEvent(npc);
                walkEvent.setBreakCondition(new Condition() {
                    @Override
                    public boolean evaluate() {
                        return api.myPlayer().getPosition().distance(npc.getPosition()) <= maxNpcDist - 2;
                    }
                });
                api.execute(walkEvent);
            }
        }
    }

    private boolean isDeathAnimating(Character character) {
        return character.getHealthPercent() == 0 && character.isAnimating();
    }

    public String getStatus() {
        return status;
    }
}
