package flamo.MonkKiller.src.util.data;

import org.osbot.rs07.api.map.Area;

/**
 * Created by Danny on 8/8/2017.
 */

public enum Location {

    EDGEVILLE_MONASTERY(new Area(3042, 3481, 3061, 3500)),
    HOSIDIUS_MONASTERY(new Area(1729, 3497,1748, 3488));

    private String name;
    private Area area;

    Location(Area area) {
        this.area = area;
        int nextIndex = name().indexOf("_");
        this.name = (name().charAt(0) + name().substring(1, nextIndex).toLowerCase() + name().substring(nextIndex++, nextIndex + 1).toUpperCase() + name().substring(nextIndex + 1).toLowerCase()).replaceAll("_", " ");
    }

    public Area getArea() {
        return area;
    }

    public String getName() {
        return name;
    }

}
